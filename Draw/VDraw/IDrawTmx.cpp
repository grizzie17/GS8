/*---------------------------------------------------------------------+\
|
|	IDrawTmx.cpp  --  define IXID value for IDrawTmx
|
|	Purpose:
|
| optional topics (delete if not used)
|	Restrictions/Warnings:
|	Formats:
|	References:
|	Notes:
|
\+---------------------------------------------------------------------*/
/*---------------------------------------------------------------------+\
|
|	Revision History:					(most recent entries first)
|
	dd-mmm-9999			J.Griswold		(Reviewed by: xxxx)
		Initial Revision
|
\+---------------------------------------------------------------------*/
/*---------------------------------------------------------------------+\
|					
|	Include Files	
|					
\+---------------------------------------------------------------------*/
#include "IDrawTmx.h"

/*---------------------------------------------------------------------+\
|				
|	Namespace	
|				
\+---------------------------------------------------------------------*/
NAMESPACE_COMMON_BEGIN
/*---------------------------------------------------------------------+\
|						
|	Interface Constants	
|						
\+---------------------------------------------------------------------*/

const IXID	IXID_IDrawTmx( "JD::Gadget::IDrawTmx" );
const IXID	IXID_IDrawTmx2( "JD::Gadget::IDrawTmx2" );



NAMESPACE_COMMON_END

