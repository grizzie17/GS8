/*---------------------------------------------------------------------+\
|
|	IEssStatus.h  --  define IXID value for IEssStatus
|
|	Purpose:
|	File Custodian:		F.Lastname
|
| optional topics (delete if not used)
|	Restrictions/Warnings:
|	Formats:
|	References:
|	Notes:
|
\+---------------------------------------------------------------------*/
/*---------------------------------------------------------------------+\
|
|	Revision History:					(most recent entries first)
|
	dd-mmm-9999			F.Lastname		(Reviewed by: xxxx)
		Initial Revision
|
\+---------------------------------------------------------------------*/
/*---------------------------------------------------------------------+\
|																		|
|	Include Files														|
|																		|
\+---------------------------------------------------------------------*/
#include "IEssStatus.h"

/*---------------------------------------------------------------------+\
|																		|
|	Namespace															|
|																		|
\+---------------------------------------------------------------------*/
NAMESPACE_GADGET_BEGIN
/*---------------------------------------------------------------------+\
|																		|
|	Interface Constants													|
|																		|
\+---------------------------------------------------------------------*/

const IXID	IXID_IEssStatus( "DRS::Gadget::IEssStatus" );



NAMESPACE_GADGET_END

