/*---------------------------------------------------------------------+\
|
|	IDictionaryUser.cpp  --  define IXID value for IDictionaryUser
|
|	Purpose:
|
| optional topics (delete if not used)
|	Restrictions/Warnings:
|	Formats:
|	References:
|	Notes:
|
\+---------------------------------------------------------------------*/
/*---------------------------------------------------------------------+\
|
|	Revision History:					(most recent entries first)
|
	24-Oct-2010			J.Griswold		(Reviewed by: xxxx)
		Initial Revision
|
\+---------------------------------------------------------------------*/
/*---------------------------------------------------------------------+\
|																		|
|	Include Files														|
|																		|
\+---------------------------------------------------------------------*/
#include "stdafx.h"
#include "IDictionaryUser.h"

/*---------------------------------------------------------------------+\
|																		|
|	Namespace															|
|																		|
\+---------------------------------------------------------------------*/
NAMESPACE_GADGET_BEGIN
//USING_NAMESPACE_COMMON
/*---------------------------------------------------------------------+\
|																		|
|	Interface Constants													|
|																		|
\+---------------------------------------------------------------------*/

const IXID	IXID_IDictionaryUser( "BCG::Gadget::IDictionaryUser" );



NAMESPACE_GADGET_END

