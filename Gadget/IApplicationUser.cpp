/*---------------------------------------------------------------------+\
|
|	IApplicationUser.cpp  --  define IXID value for IApplicationUser
|
|	Purpose:
|
| optional topics (delete if not used)
|	Restrictions/Warnings:
|	Formats:
|	References:
|	Notes:
|
\+---------------------------------------------------------------------*/
/*---------------------------------------------------------------------+\
|
|	Revision History:					(most recent entries first)
|
	24-Oct-2010			J.Griswold		(Reviewed by: xxxx)
		Initial Revision
|
\+---------------------------------------------------------------------*/
/*---------------------------------------------------------------------+\
|																		|
|	Include Files														|
|																		|
\+---------------------------------------------------------------------*/
#include "stdafx.h"
#include "IApplicationUser.h"

/*---------------------------------------------------------------------+\
|																		|
|	Namespace															|
|																		|
\+---------------------------------------------------------------------*/
NAMESPACE_GADGET_BEGIN
/*---------------------------------------------------------------------+\
|																		|
|	Interface Constants													|
|																		|
\+---------------------------------------------------------------------*/

const IXID	IXID_IApplicationUser( "BCG::Gadget::IApplicationUser" );



NAMESPACE_GADGET_END

