/*---------------------------------------------------------------------+\
|
|	IEventsKeyboard.h  --  define IXID value for IEventsKeyboard
|
|	Purpose:
|
| optional topics (delete if not used)
|	Restrictions/Warnings:
|	Formats:
|	References:
|	Notes:
|
\+---------------------------------------------------------------------*/
/*---------------------------------------------------------------------+\
|
|	Revision History:					(most recent entries first)
|
	05-Nov-2011			J.Griswold		(Reviewed by: xxxx)
		Initial Revision
|
\+---------------------------------------------------------------------*/
/*---------------------------------------------------------------------+\
|					
|	Include Files	
|					
\+---------------------------------------------------------------------*/
#include "IEventsKeyboard.h"

/*---------------------------------------------------------------------+\
|				
|	Namespace	
|				
\+---------------------------------------------------------------------*/
NAMESPACE_GADGET_BEGIN
/*---------------------------------------------------------------------+\
|						
|	Interface Constants	
|						
\+---------------------------------------------------------------------*/

const IXID	IXID_IEventsKeyboard( "JD::Gadget::IEventsKeyboard" );



NAMESPACE_GADGET_END

