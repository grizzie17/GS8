/*---------------------------------------------------------------------+\
|
|	IActiveXML.h  --  define IXID value for IActiveXML
|
|	File Custodian:		F.Lastname
|
| optional topics (delete if not used)
|	Restrictions/Warnings:
|	Formats:
|	References:
|	Notes:
|
\+---------------------------------------------------------------------*/
/*---------------------------------------------------------------------+\
|
|	Revision History:					(most recent entries first)
|
	dd-mmm-9999			F.Lastname		(Reviewed by: xxxx)
		Initial Revision
|
\+---------------------------------------------------------------------*/
/*---------------------------------------------------------------------+\
|																		|
|	Include Files														|
|																		|
\+---------------------------------------------------------------------*/
#include "IActiveXML.h"

/*---------------------------------------------------------------------+\
|																		|
|	Namespace															|
|																		|
\+---------------------------------------------------------------------*/
NAMESPACE_GADGET_BEGIN
/*---------------------------------------------------------------------+\
|																		|
|	Interface Constants													|
|																		|
\+---------------------------------------------------------------------*/

const IXID	IXID_IActiveXML( "BCG::Gadget::IActiveXML" );
const IXID	IXID_IActiveXMLClient( "BCG::Gadget::IActiveXMLClient" );



NAMESPACE_GADGET_END

