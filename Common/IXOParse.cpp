/*---------------------------------------------------------------------+\
|
|	IXOParse.h  --  define IXID value for IXOParse
|
|	Purpose:
|
| optional topics (delete if not used)
|	Restrictions/Warnings:
|	Formats:
|	References:
|	Notes:
|
\+---------------------------------------------------------------------*/
/*---------------------------------------------------------------------+\
|
|	Revision History:					(most recent entries first)
|
	18-Oct-2010			J.Griswold		(Reviewed by: xxxx)
		Initial Revision
|
\+---------------------------------------------------------------------*/
/*---------------------------------------------------------------------+\
|																		|
|	Include Files														|
|																		|
\+---------------------------------------------------------------------*/
#include "stdafx.h"
#include "IXOParse.h"

/*---------------------------------------------------------------------+\
|																		|
|	Namespace															|
|																		|
\+---------------------------------------------------------------------*/
NAMESPACE_COMMON_BEGIN
/*---------------------------------------------------------------------+\
|																		|
|	Interface Constants													|
|																		|
\+---------------------------------------------------------------------*/

const IXID	IXID_IXOParse( "BCG::Common::IXOParse" );
const IXID	IXID_IXOParseType( "BCG::Common::IXOParseType" );
const IXID	IXID_IXOParseAttribute( "BCG::Common::IXOParseAttribute" );
const IXID	IXID_IXOParseProperty( "BCG::Common::IXOParseProperty" );
const IXID	IXID_IXOParseNProperty( "BCG::Common::IXOParseNProperty" );
const IXID	IXID_IXOParseSet( "BCG::Common::IXOParseSet" );
const IXID	IXID_IXOParseRoot( "BCG::Common::IXOParseRoot" );




NAMESPACE_COMMON_END

