/*---------------------------------------------------------------------+\
|
|	IExternalXML.h  --  define IXID value for IExternalXML
|
|	Purpose:
|
| optional topics (delete if not used)
|	Restrictions/Warnings:
|	Formats:
|	References:
|	Notes:
|
\+---------------------------------------------------------------------*/
/*---------------------------------------------------------------------+\
|
|	Revision History:					(most recent entries first)
|
	21-Oct-2010			J.Griswold		(Reviewed by: xxxx)
		Initial Revision
|
\+---------------------------------------------------------------------*/
/*---------------------------------------------------------------------+\
|																		|
|	Include Files														|
|																		|
\+---------------------------------------------------------------------*/
#include "stdafx.h"
#include "IExternalXML.h"

/*---------------------------------------------------------------------+\
|																		|
|	Namespace															|
|																		|
\+---------------------------------------------------------------------*/
NAMESPACE_COMMON_BEGIN
/*---------------------------------------------------------------------+\
|																		|
|	Interface Constants													|
|																		|
\+---------------------------------------------------------------------*/

const IXID	IXID_IExternalXML( "BCG::Common::IExternalXML" );
const IXID	IXID_IExternalXMLServer( "BCG::Common::IExternalXMLServer" );



NAMESPACE_COMMON_END

